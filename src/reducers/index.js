import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import login from '../pages/Login/reducer';
import products from '../pages/Products/reducer';
import projects from '../pages/Projects/reducer';
import projectDetails from '../pages/ProjectDetails/reducer';
import businessIdea from '../pages/BusinessIdea/reducer';
import tribeZero from '../pages/TribeZero/reducer';
import { snackbar } from '../components/elements/Snackbar/reducer';
import { sideDrawerData } from '../components/elements/SideDrawer/reducer';

const rootReducer = combineReducers({
  form: formReducer,
  login,
  products,
  projects,
  projectDetails,
  businessIdea,
  tribeZero,
  routing: routerReducer,
  snackbar,
  sideDrawer: sideDrawerData,
});

export default rootReducer;
