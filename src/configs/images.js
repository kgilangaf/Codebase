const images = {
  LOGO: '/assets/codex-logo.svg',
  LOGO_TELKOM: '/assets/codex-logo-telkom.svg',
  LOGO_X: '/assets/codex-x.svg',
  BUTTON_YES: '/assets/button-yes.png',
  BUTTON_NO: '/assets/button-no.png',
  LOGIN_BG: '/assets/login-bg.png',
  SIGNUP_BG: '/assets/sign-up-bg.png',
  ILLUSTRATION_BG: '/assets/assessment-tools-illustration-incloud.svg',
  SIGNUP_ILLUSTRATION: '/assets/illustrasi-signup.svg',
  LOGIN_BACKGROUND: '/assets/login-backgrounds.svg',
  BG_LOGIN: '/assets/bg-login.png',
};

export default images;
