const routes = {
  HOME() {
    return `/`;
  },
  LOGIN() {
    return `/login`;
  },
  PROJECTS() {
    return `/project`;
  },
  PROJECT_DETAILS(param) {
    return `/project/details/${param}`;
  },
  BUSINESS_IDEA() {
    return `/business-idea`;
  },
  BUSINESS_IDEA_FORM(param) {
    return `/business-idea/form/${param}`;
  },
  TRIBE_ZERO() {
    return `/tribe-zero`;
  },
  TRIBE_ZERO_COMMENT(param) {
    return `/tribe-zero/comment/${param}`;
  },
};

export default routes;
