export const errorHandler = {
  maxLength: function(max) {
    return `Maximum ${max} characters`;
  },
  minLength: function(min) {
    return `Minimum ${min} characters`;
  },
  required: function(title) {
    return `${title} field is required`;
  },
  maxSize: function(type, size) {
    return `Maximum upload ${type} size ${size} MB`;
  },
  integer: function() {
    return `Only numbers are allowed`;
  },
  maxDigit: function(title, max) {
    return `${title} maksimal ${max} digit`;
  },
  email: function() {
    return `Email must be entered in example@xxx.xxx format`;
  },
};

export const placeholder = {
  shortText: function(label, command = 'type') {
    if (label === '') return `You can ${command} here`;
    return `You can ${command} ${label} here`;
  },
  select: function(label) {
    return `Please select the ${label}`;
  },
  longText: function(label, max) {
    return `You can type ${label} here (${max} characters maximum)`;
  },
  file: function(type, max) {
    return `Add ${type} (${max} MB maximum)`;
  },
};
