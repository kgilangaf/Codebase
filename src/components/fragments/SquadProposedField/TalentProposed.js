import React from 'react';
import PropTypes from 'prop-types';
import FieldSelect from '../../elements/FieldSelect';
import FieldText from '../../elements/FieldText';
import { Grid } from '@material-ui/core';
import { Field, getIn } from 'formik';
import { placeholder } from '../../../constants/copywriting';
import Button from '../../elements/Button';
import TalentExisting from './TalentExisting';
import { ICONS } from '../../../configs';

export default class Component extends React.Component {
  addTalentExistingField(totalExisting, talIdx, squadIdx) {
    const { field, setFieldValue, setFieldTouched } = this.props;

    let talentData = field[talIdx].talentExisting;
    let remainData = totalExisting - talentData.length;

    if (remainData > 0) {
      for (let i = 0; i < remainData; i++) {
        talentData.push({
          name: '',
          email: '',
        });
      }
    } else if (remainData < 0) {
      const totalUpdatedData = talentData.length + remainData;
      for (let i = talentData.length - 1; i >= totalUpdatedData; i--) {
        setFieldTouched(
          `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${i}].name`,
          false,
          false,
        );
        setFieldTouched(
          `squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting[${i}].email`,
          false,
          false,
        );
      }
      talentData.splice(totalUpdatedData);
    }

    setFieldValue(`squadRequest[${squadIdx}].talentRequest[${talIdx}].talentExisting`, talentData);
  }

  render() {
    const {
      jobRoleOptions,
      jobLevelOptions,
      field,
      disabled,
      errors,
      touched,
      arrayHelpers,
      squadIdx,
      setFieldTouched,
      setFieldValue,
      values,
    } = this.props;

    let submit = [];

    return (
      <>
        {field.map((item, talIdx) => (
          <>
            <Grid className="field-group" container key={talIdx}>
              <Field
                name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].jobRole`}
                render={({ field }) => (
                  <FieldSelect
                    {...field}
                    disabled={disabled}
                    error={getIn(
                      errors,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobRole`,
                    )}
                    label="Job Role"
                    onBlur={() =>
                      setFieldTouched(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobRole`,
                        true,
                      )
                    }
                    onChange={(e) => {
                      setFieldValue(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobRole`,
                        e,
                      );
                    }}
                    options={jobRoleOptions}
                    placeholder={placeholder.select('job role')}
                    required
                    touched={getIn(
                      touched,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobRole`,
                    )}
                    withSearch
                  />
                )}
              />
              <Field
                name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].jobLevel`}
                render={({ field }) => (
                  <FieldSelect
                    {...field}
                    disabled={disabled}
                    error={getIn(
                      errors,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobLevel`,
                    )}
                    label="Job Level"
                    onBlur={() =>
                      setFieldTouched(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobLevel`,
                        true,
                      )
                    }
                    onChange={(e) => {
                      setFieldValue(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobLevel`,
                        e,
                      );
                    }}
                    options={jobLevelOptions}
                    placeholder={placeholder.select('job level')}
                    required
                    touched={getIn(
                      touched,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].jobLevel`,
                    )}
                  />
                )}
              />
              <Field
                name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].amountSubmitted`}
                render={({ field }) => (
                  <FieldText
                    {...field}
                    disabled={disabled}
                    error={getIn(
                      errors,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountSubmitted`,
                    )}
                    label="Number of Proposed Talent"
                    max={2}
                    onChange={(e) => {
                      setFieldTouched(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountSubmitted`,
                        true,
                      );
                      submit[talIdx] = parseInt(e.target.value);
                      setFieldValue(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountSubmitted`,
                        parseInt(e.target.value),
                      );
                    }}
                    placeholder={placeholder.shortText('')}
                    required
                    style={{ width: '13.5rem' }}
                    touched={getIn(
                      touched,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountSubmitted`,
                    )}
                    type="number"
                  />
                )}
              />
              <Field
                name={`squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`}
                render={({ field }) => (
                  <FieldText
                    {...field}
                    disabled={disabled}
                    error={getIn(
                      errors,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`,
                    )}
                    info="Data of proposed and existing talent are integrated."
                    label="Number of Existing Talent"
                    max={2}
                    onBlur={(e) => {
                      this.addTalentExistingField(e.target.value, talIdx, squadIdx);
                      setFieldTouched(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`,
                        true,
                      );
                    }}
                    onChange={async (e) => {
                      const submittedValue =
                        isNaN(
                          values.squadRequest[squadIdx].talentRequest[talIdx].amountSubmitted,
                        ) ||
                        parseInt(e.target.value) >
                          values.squadRequest[squadIdx].talentRequest[talIdx].amountSubmitted
                          ? values.squadRequest[squadIdx].talentRequest[talIdx].amountSubmitted
                          : parseInt(e.target.value);
                      setFieldTouched(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`,
                        true,
                      );
                      await setFieldValue(
                        `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`,
                        submittedValue,
                      );
                    }}
                    placeholder={placeholder.shortText('')}
                    required
                    style={{ width: '13.5rem' }}
                    touched={getIn(
                      touched,
                      `squadRequest[${squadIdx}].talentRequest[${talIdx}].amountExisting`,
                    )}
                    type="number"
                  />
                )}
              />
              {field.length > 1 && !disabled && (
                // Button Delete Talent
                <Button
                  circle
                  onClick={() => arrayHelpers.remove(talIdx)}
                  small
                  style={{
                    marginLeft: '-1rem',
                    height: '1rem',
                    backgroundColor: '#00000000',
                    transform: `translate(0, 1.75rem)`,
                  }}
                >
                  <img src={ICONS.MINUS_ICON} style={{ width: '1rem', height: '1rem' }} />
                </Button>
              )}
            </Grid>
            {field[talIdx].talentExisting.length > 0 && (
              <TalentExisting
                disabled={disabled}
                errors={errors}
                field={field[talIdx].talentExisting}
                setFieldValue={setFieldValue}
                squadIdx={squadIdx}
                talIdx={talIdx}
                touched={touched}
              />
            )}
          </>
        ))}
        {/* Add Talent */}
        {!disabled && (
          <Button
            flat
            onClick={() =>
              arrayHelpers.push({
                jobRole: '',
                jobLevel: '',
                amountSubmitted: '',
                amountExisting: '',
                talentExisting: [],
              })
            }
            style={{ marginBottom: '0', paddingLeft: 0, outline: 'none' }}
          >
            + Add Proposed Talent
          </Button>
        )}
      </>
    );
  }
}

Component.defaultProps = {
  className: '',
  disabled: false,
  squadIdx: 0,
};

Component.propTypes = {
  arrayHelpers: PropTypes.object.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  errors: PropTypes.object.isRequired,
  field: PropTypes.array.isRequired,
  jobLevelOptions: PropTypes.array.isRequired,
  jobRoleOptions: PropTypes.array.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  squadIdx: PropTypes.number,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
