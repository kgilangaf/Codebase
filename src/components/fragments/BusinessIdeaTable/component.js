import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../elements/Pagination';
import styles from './styles.module.scss';
import EditIcon from '../../icons/Edit';
import DownloadIcon from '../../icons/Download';
import TrashIcon from '../../icons/Trash';
import { Grid } from '@material-ui/core';
import stylesTable from '../../../styles/_table.module.scss';
import Tooltip from '../../elements/Tooltip';
import Button from '../../elements/Button';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '../../elements/Dialog';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      id: null,
    };
    this._handleToggleDialog = this._handleToggleDialog.bind(this);
    this._handleOnDelete = this._handleOnDelete.bind(this);
  }

  _handleOnEdit(e, id) {
    e.preventDefault();
    this.props.onEditItem(id);
  }

  _handleOnDelete() {
    this.props.onDeleteItem(this.state.id).then(() => {
      this.setState({
        id: null,
        isOpen: !this.state.isOpen,
      });
    });
  }

  _handleOnDownload(e, id) {
    e.preventDefault();
    this.props.onDownloadItem(id);
  }

  _handleToggleDialog(e, id = null) {
    e.preventDefault();
    this.setState({
      isOpen: !this.state.isOpen,
      id,
    });
  }

  _handleDisabledAction(actName, status) {
    // status === status Code

    let isDisabled = false;

    switch (actName) {
      case 'edit':
        if (status === null || status === 3 || status === 5) isDisabled = false;
        else isDisabled = true;
        break;
      case 'download':
        if (status === null) isDisabled = true;
        else isDisabled = false;
        break;
      case 'delete':
        if (status === null || status === 3 || status === 5 || status === 8) isDisabled = false;
        else isDisabled = true;
        break;
    }

    return isDisabled;
  }

  render() {
    return (
      <>
        <Dialog
          aria-describedby="alert-dialog-description"
          aria-labelledby="alert-dialog-title"
          onClose={this._handleToggleDialog}
          open={this.state.isOpen}
        >
          <DialogTitle id="alert-dialog-title">Do you want to delete the proposal?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              This process can not be undone
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button ghost onClick={this._handleToggleDialog} type="button">
              Cancel
            </Button>
            <Button autoFocus onClick={this._handleOnDelete} type="button">
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <table className={stylesTable['cdx-table']} id={styles['business-idea-table']}>
          <thead>
            <tr>
              <th>Name of Idea</th>
              <th>Date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody className={styles.tbody}>
            {this.props.data &&
              this.props.data.map((row, index) => (
                <tr key={index}>
                  <td>
                    {row.name ? (
                      <Tooltip placement="bottom-start" title={row.name}>
                        <span>{row.name}</span>
                      </Tooltip>
                    ) : null}
                  </td>
                  <td>{row.updated_at}</td>
                  <td>{row.status}</td>
                  <td className={styles['td-action']}>
                    <Grid alignItems="baseline" container direction="row" justify="space-evenly">
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('edit', row.status_code)}
                          onClick={(e) => this._handleOnEdit(e, row.id)}
                        >
                          <EditIcon
                            color={
                              this._handleDisabledAction('edit', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Edit
                        </button>
                      </Grid>
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('download', row.status_code)}
                          onClick={(e) => this._handleOnDownload(e, row.id)}
                        >
                          <DownloadIcon
                            color={
                              this._handleDisabledAction('download', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Download
                        </button>
                      </Grid>
                      <Grid item>
                        <button
                          disabled={this._handleDisabledAction('delete', row.status_code)}
                          onClick={(e) => this._handleToggleDialog(e, row.id)}
                        >
                          <TrashIcon
                            color={
                              this._handleDisabledAction('delete', row.status_code)
                                ? '#dee3ed'
                                : '#1E2025'
                            }
                          />
                          Delete
                        </button>
                      </Grid>
                    </Grid>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        {this.props.data && (
          <Pagination
            className={styles.pagination}
            meta={this.props.meta}
            onChangeUrl={this.props.onChangeUrl}
          />
        )}
      </>
    );
  }
}

Component.defaultProps = {
  match: {},
  onChangeUrl: {},
};

Component.propTypes = {
  data: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.object,
  meta: PropTypes.object.isRequired,
  onChangeUrl: PropTypes.func,
  onDeleteItem: PropTypes.func.isRequired,
  onDownloadItem: PropTypes.func.isRequired,
  onEditItem: PropTypes.func.isRequired,
};
