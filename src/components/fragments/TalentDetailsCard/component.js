import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

export default class Component extends React.Component {
  render() {
    const { data, isLoading } = this.props;
    
    if(isLoading || !data.length)
      return null;
    
    return (
      <table className={styles.table}>
        <thead>
          <tr>
            <th>Talent</th>
            <th>Job Role</th>
            <th className={styles['th-level']}>Level</th>
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          {data.map((item, idx) => (
            <tr key={idx}>
              <td>{item.talentName}</td>
              <td>{item.talentRole}</td>
              <td className={styles['td-level']}>{item.talentLevel}</td>
            </tr>  
          ))}
        </tbody>
      </table>
    );
  }
}

Component.defaultProps = {
  data: [],
  isLoading: false,
  match: {},
};

Component.propTypes = {
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  match: PropTypes.object,
};
