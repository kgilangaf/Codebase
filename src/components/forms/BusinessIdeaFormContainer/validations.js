import { errorHandler } from '../../../constants/copywriting';
import { REGEX_EMAIL_MAXCHAR, REGEX_EMAIL_CHARACTER } from '../../../constants';

import * as Yup from 'yup';

export default Yup.object().shape({
  // === page 1
  name: Yup.string()
    .required(errorHandler.required('Name of Business Idea'))
    .max(255, errorHandler.maxLength(255)),
  productType: Yup.string().required(errorHandler.required('Type of Product')),
  productName: Yup.string()
    .max(255, errorHandler.maxLength(255))
    .when('productType', {
      is: (val) => val === '1' || val === '2',
      then: Yup.string()
        .required(errorHandler.required('Name of Product'))
        .max(255, errorHandler.maxLength(255)),
    }),
  type: Yup.string().required(errorHandler.required('Type of Business Idea')),
  area: Yup.string().required(errorHandler.required('Area of Business Idea')),
  businessField: Yup.array()
    .required(errorHandler.required('Field of Business Idea'))
    .of(Yup.string().max(160, errorHandler.maxLength(160))),
  customer: Yup.string().required(errorHandler.required('Customer of Business Idea')),
  otherCustomer: Yup.string().when('customer', {
    is: 'Other',
    then: Yup.string()
      .required(errorHandler.required('Customer of Business Idea'))
      .max(160, errorHandler.maxLength(160)),
  }),

  // === page 2
  backgroundPlain: Yup.string()
    .required(errorHandler.required('Background'))
    .max(700, errorHandler.maxLength(700)),
  descPlain: Yup.string()
    .required(errorHandler.required('Description of Business Idea'))
    .max(700, errorHandler.maxLength(700)),
  telkomBenefitPlain: Yup.string()
    .required(errorHandler.required('Benefit for Telkom'))
    .max(700, errorHandler.maxLength(700)),
  customerBenefitPlain: Yup.string()
    .required(errorHandler.required('Benefit for Customer'))
    .max(700, errorHandler.maxLength(700)),
  valueCapturePlain: Yup.string()
    .required(errorHandler.required('Value Capture'))
    .max(700, errorHandler.maxLength(700)),

  // === page 3
  goldenCircleUrl: Yup.string().required(errorHandler.required('Golden Circle')),
  goldenCircleWhy: Yup.string()
    .required(errorHandler.required('Why'))
    .max(700, errorHandler.maxLength(700)),
  goldenCircleHow: Yup.string()
    .required(errorHandler.required('How'))
    .max(700, errorHandler.maxLength(700)),
  goldenCircleWhat: Yup.string()
    .required(errorHandler.required('What'))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerUrl: Yup.string().required(errorHandler.required(`Value Proposition Canvas`)),
  valuePropCustomerJobs: Yup.string()
    .required(errorHandler.required(`Customer's Job`))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerPain: Yup.string()
    .required(errorHandler.required(`Customer's Pain`))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerGain: Yup.string()
    .required(errorHandler.required(`Customer's Gain`))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerProductService: Yup.string()
    .required(errorHandler.required(`Product & Services`))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerPainRelievers: Yup.string()
    .required(errorHandler.required(`Pain Relievers`))
    .max(700, errorHandler.maxLength(700)),
  valuePropCustomerGainCreators: Yup.string()
    .required(errorHandler.required(`Gain Creators`))
    .max(700, errorHandler.maxLength(700)),
  additionalCanvasName: Yup.string().required(errorHandler.required('Additional Canvassing')),
  leanCanvasUrl: Yup.string().when('additionalCanvasName', {
    is: 'Lean Canvas',
    then: Yup.string().required(errorHandler.required('Lean Canvas')),
  }),
  businessModelUrl: Yup.string().when('additionalCanvasName', {
    is: 'Business Model Canvas',
    then: Yup.string().required(errorHandler.required('Lean Canvas')),
  }),
  additionalCanvasDesc: Yup.string()
    .required(errorHandler.required('Explanation'))
    .max(700, errorHandler.maxLength(700)),

  // === page 4
  execDate: Yup.object().required(errorHandler.required('Time of Implementation')),
  omtm: Yup.string()
    .required(errorHandler.required('Metric OMTM'))
    .max(160, errorHandler.maxLength(160)),
  omtmDesc: Yup.string()
    .required(errorHandler.required('Explanation'))
    .max(700, errorHandler.maxLength(700)),
  omtmTarget: Yup.number()
    .typeError(errorHandler.integer())
    .required(errorHandler.required('Target')),

  // === page 5
  squadRequest: Yup.array().of(
    Yup.object().shape({
      squadName: Yup.string()
        .required(errorHandler.required('Name of Squad'))
        .max(32, errorHandler.maxLength(32)),
      talentRequest: Yup.array().of(
        Yup.object().shape({
          jobRole: Yup.string().required(errorHandler.required('Job Role')),
          jobLevel: Yup.string().required(errorHandler.required('Job Level')),
          amountSubmitted: Yup.number()
            .required(errorHandler.required('Number of Proposed Talent'))
            .typeError(errorHandler.required('Number of Proposed Talent'))
            .min(1, 'Input 1 talent minimum'),
          amountExisting: Yup.number()
            .notRequired()
            .typeError(errorHandler.required('Number of Existing Talent'))
            .test(
              'smallerThanSubmitted',
              `Must be less or same than number of Proposed Talent`,
              function(value) {
                const amountSubmitted = this.options.parent.amountSubmitted;
                if (isNaN(amountSubmitted)) return true;
                if (value <= amountSubmitted) return true;
                return false;
              },
            ),
          talentExisting: Yup.array().of(
            Yup.object().shape({
              name: Yup.string()
                .required(errorHandler.required('Name of Existing Talent'))
                .max(32, errorHandler.maxLength(32)),
              email: Yup.string()
                .required(errorHandler.required('Email Address'))
                .max(255, errorHandler.maxLength(255))
                .email(errorHandler.email())
                .test(
                  'usernameMaxChar',
                  'Only 3-30 characters are required before @xxx.xxx',
                  (value) => {
                    if (value.indexOf('@') !== -1) {
                      const username = value.substr(0, value.indexOf('@'));
                      if (REGEX_EMAIL_MAXCHAR.test(username)) return true;
                      return false;
                    } else {
                      return true;
                    }
                  },
                )
                .test(
                  'specialChar',
                  'Only letters (a-z), numbers, and periods are allowed',
                  (value) => {
                    if (value.indexOf('@') !== -1) {
                      const username = value.substr(0, value.indexOf('@'));
                      if (REGEX_EMAIL_CHARACTER.test(username)) return true;
                      return false;
                    } else {
                      return true;
                    }
                  },
                ),
            }),
          ),
        }),
      ),
    }),
  ),
  costTalent: Yup.number()
    .typeError(errorHandler.required('Cost Talent'))
    .required(errorHandler.required('Cost Talent')),
  costEnabler: Yup.number()
    .typeError(errorHandler.required('Cost Enabler'))
    .required(errorHandler.required('Cost Enabler')),
  rewardForTalent: Yup.number()
    .typeError(errorHandler.required('Reward for Talent'))
    .required(errorHandler.required('Reward for Talent')),
  development: Yup.number()
    .typeError(errorHandler.required('Development'))
    .required(errorHandler.required('Development')),
  partnership: Yup.number()
    .typeError(errorHandler.required('Partnership'))
    .required(errorHandler.required('Partnership')),
  operational: Yup.number()
    .typeError(errorHandler.required('Operational'))
    .required(errorHandler.required('Operational')),
});
