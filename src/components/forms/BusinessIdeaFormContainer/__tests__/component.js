import React from 'react';
import { shallow } from 'enzyme';
import Component from '../component';
import initialData from '../../../../pages/BusinessIdeaForm/initialData';

describe('Business Idea Form', () => {
  it('Form renders correctly', () => {
    const attrs = {
      handleSendForm: jest.fn(),
      id: 1,
      onFilesAdded: jest.fn(),
      onFilesRemoved: jest.fn(),
      openSnackbar: jest.fn(),
      handleAutoSave: jest.fn()
    };

    const wrapper = shallow(<Component {...initialData} {...attrs} />);
    expect(wrapper).toBeCalled;
  });
});
