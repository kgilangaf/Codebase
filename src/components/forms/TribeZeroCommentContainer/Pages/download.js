import React from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import Link from '../../../elements/Link';
import Button from '../../../elements/Button';
import { ROUTES, SERVICES } from '../../../../configs';
import axios from 'axios';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._handleDownloadPdf = this._handleDownloadPdf.bind(this);
    this.state = {
      isloading: false,
    };
  }

  _handleDownloadPdf() {
    this.setState({ isloading: true });
    axios
      .get(SERVICES.GET_TRIBE_ZERO_PDF(this.props.values.id))
      .then(() => {
        this.setState({ isloading: false });
        window.open(SERVICES.GET_TRIBE_ZERO_PDF(this.props.values.id), '_blank');
      })
      .catch(() => {
        setTimeout(() => {
          axios
            .get(SERVICES.GET_TRIBE_ZERO_PDF(this.props.values.id))
            .then(() => {
              this.setState({ isloading: false });
              window.open(SERVICES.GET_TRIBE_ZERO_PDF(this.props.values.id), '_blank');
            })
            .catch(() => {
              this.setState({ isloading: false });
              alert('there is something error');
            });
        }, 500);
      });
  }

  render() {
    return (
      <Grid alignItems="center" container direction="column">
        <h3>The form has been completed!</h3>
        <p style={{ maxWidth: '50vw', textAlign: 'center' }}>
          Thank you for filling out the evaluation of business idea proposal. The evaluation of
          proposal has been sent to Business Unit. To access a summary of the form that you have
          filled in, please visit the main page or download the following document:
        </p>
        <Button isloading={this.state.isloading} onClick={() => this._handleDownloadPdf()}>
          Download Document
        </Button>
        <p>
          To access the main page click <Link path={ROUTES.TRIBE_ZERO()}>here</Link>.
        </p>
      </Grid>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
