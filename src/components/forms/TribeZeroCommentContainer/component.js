/* eslint-disable max-lines */
import React from 'react';
import Button from '../../elements/Button';
import { withFormik } from 'formik';
import validations from './validations';
import General from './Pages/p1General';
import ExecutiveSummary from './Pages/p2ExecutiveSummary';
import GoldenCircle from './Pages/p3GoldenCircle';
import ValuePropositionCanvas from './Pages/p4ValuePropositionCanvas';
import AdditionalCanvassing from './Pages/p5AdditionalCanvassing';
import TimeAndMetric from './Pages/p6TimeAndMetric';
import ProposedTalent from './Pages/p7ProposedTalent';
import ProposedBudget from './Pages/p8ProposedBudget';
import Summary from './Pages/p9Summary';
import PitchingSchedule from './Pages/p10PitchingSchedule';
import DownloadPage from './Pages/download';
import propTypes from 'prop-types';
import _ from 'lodash';
import { Grid } from '@material-ui/core';
import PromptModal from '../../fragments/TribeZeroFormModal';

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this._handleAutosave = _.debounce(this._handleAutosave, 2000);
    this._handleToggleDialog = this._handleToggleDialog.bind(this);
    this._handleConfirmationCopywriting = this._handleConfirmationCopywriting.bind(this);
    this._handleBeforeSubmitting = this._handleBeforeSubmitting.bind(this);
    this._handleSubmitData = this._handleSubmitData.bind(this);
    this.state = {
      page: 1,
      lastPageReject: 9,
      lastPageAccept: 10,
      downloadPage: 11,
      isOpen: false,
      draftLoading: false,
      submitCopy: {
        title: 'Do you want to reject the proposal?',
        subtitle: 'This evaluation and the proposal that you reject will be sent to proposer.',
      },
    };
  }

  _handleAutosave() {
    //handle autosave
  }

  _findErrorPage() {
    const { errors, touched } = this.props;
    if (errors.feedbackGeneral && touched.feedbackGeneral) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 1 });
      return true;
    } else if (
      (errors.feedbackBackground && touched.feedbackBackground) ||
      (errors.feedbackDesc && touched.feedbackDesc) ||
      (errors.feedbackTelkomBenefit && touched.feedbackTelkomBenefit) ||
      (errors.feedbackCustomerBenefit && touched.feedbackCustomerBenefit) ||
      (errors.feedbackValueCapture && touched.feedbackValueCapture)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });

      this.setState({ page: 2 });
      return true;
    } else if (errors.feedbackGoldenCircle && touched.feedbackGoldenCircle) {
      window.scrollTo({ top: 0, behavior: 'smooth' });

      this.setState({ page: 3 });
      return true;
    } else if (errors.feedbackValuePropCustomer && touched.feedbackValuePropCustomer) {
      window.scrollTo({ top: 0, behavior: 'smooth' });

      this.setState({ page: 4 });
      return true;
    } else if (errors.feedbackAdditionalCanvas && touched.feedbackAdditionalCanvas) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 5 });
      return true;
    } else if (
      (errors.feedbackOmtm && touched.feedbackOmtm) ||
      (errors.feedbackExecDate && touched.feedbackExecDate)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 6 });
      return true;
    } else if (errors.feedbackSquad && touched.feedbackSquad) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 7 });
      return true;
    } else if (errors.feedbackBudget && touched.feedbackBudget) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 8 });
      return true;
    } else if (
      (errors.feedbackSummary && touched.feedbackSummary) ||
      (errors.decision && touched.decision)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 9 });
      return true;
    } else if (
      (errors.pitchingDate && touched.pitchingDate) ||
      (errors.pitchingTimeStart && touched.pitchingTimeStart) ||
      (errors.pitchingTimeEnd && touched.pitchingTimeEnd) ||
      (errors.address && touched.address) ||
      (errors.tribe && touched.tribe) ||
      (errors.otherTribe && touched.otherTribe)
    ) {
      this.setState({ page: 10 });
    }
    return false;
  }

  _handleSaveDraft() {
    const isError = this._findErrorPage();
    if (!isError) {
      const { handleSendForm, values } = this.props;
      this.setState({ draftLoading: true });
      handleSendForm(values)
        .then(() => {
          this.setState({ draftLoading: false });
          this.props.openSnackbar({
            isOpen: true,
            variant: 'success',
            message: 'The form has been succesfully saved!',
          });
        })
        .catch(() => {
          this.setState({ draftLoading: false });
          this.props.openSnackbar({
            isOpen: true,
            variant: 'error',
            message: `Server error occurred. This form can not be saved.`,
          });
        });
    } else {
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: `There is invalid data input. This form can not be saved.`,
      });
    }
  }

  _handleSubmitData() {
    const { values, handleSendForm } = this.props;
    handleSendForm(values, true)
      .then(() => {
        this.setState({ draftLoading: false, isOpen: false });
        this.props.openSnackbar({
          isOpen: true,
          variant: 'success',
          message: 'The form has been succesfully submitted!',
        });
        this.setState({ page: this.state.downloadPage });
      })
      .catch(() => {
        this.setState({ draftLoading: false });
        this.props.openSnackbar({
          isOpen: true,
          variant: 'error',
          message: `Something went wrong. This form can not be submitted!`,
        });
      });
  }

  _changePage(behavior) {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    this.setState({
      page: behavior === 'next' ? this.state.page + 1 : this.state.page - 1,
    });
  }

  async _handleBeforeSubmitting() {
    const { handleSubmit, errors } = this.props;
    await handleSubmit();
    if (Object.keys(errors).length > 0) {
      setTimeout(() => this._findErrorPage(), 1);
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: `Your form is incompleted yet. This form can not be submitted.`,
      });
    } else {
      this._handleConfirmationCopywriting();
    }
  }

  _handleConfirmationCopywriting() {
    const { decision } = this.props.values;
    if (decision === 'Accept')
      this.setState({
        submitCopy: {
          title: 'Do you want to accept the proposal?',
          subtitle:
            'By accepting this proposal, the evaluation along with this pitching schedule will be sent to Digital Investment Committee.',
        },
      });
    else
      this.setState({
        submitCopy: {
          title: 'Do you want to reject the proposal?',
          subtitle: 'This evaluation and the proposal that you reject will be sent to proposer.',
        },
      });
    this._handleToggleDialog();
  }

  _handleToggleDialog() {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const {
      values,
      errors,
      touched,
      isSubmitting,
      setFieldValue,
      setFieldTouched,
      onFilesAdded,
      onFilesRemoved,
      getTribe,
      getPitchingLocation,
    } = this.props;

    const { page, lastPageReject, lastPageAccept, downloadPage } = this.state;

    const attrs = {
      errors,
      id: this.props.id,
      setFieldTouched,
      setFieldValue,
      touched,
      values,
      handleAutosave: this._handleAutosave(),
      onFilesAdded,
      onFilesRemoved,
    };

    return (
      <>
        <PromptModal
          isOpen={this.state.isOpen}
          onHandleClose={this._handleToggleDialog}
          onHandleSubmit={this._handleSubmitData}
          subtitle={this.state.submitCopy.subtitle}
          title={this.state.submitCopy.title}
        />
        <form method="POST">
          {this.state.page === 1 && <General {...attrs} />}
          {this.state.page === 2 && <ExecutiveSummary {...attrs} />}
          {this.state.page === 3 && <GoldenCircle {...attrs} />}
          {this.state.page === 4 && <ValuePropositionCanvas {...attrs} />}
          {this.state.page === 5 && <AdditionalCanvassing {...attrs} />}
          {this.state.page === 6 && <TimeAndMetric {...attrs} />}
          {this.state.page === 7 && <ProposedTalent {...attrs} />}
          {this.state.page === 8 && <ProposedBudget {...attrs} />}
          {this.state.page === 9 && <Summary {...attrs} />}
          {this.state.page === 10 && (
            <PitchingSchedule
              {...attrs}
              getPitchingLocation={getPitchingLocation}
              getTribe={getTribe}
            />
          )}
          {page === downloadPage && <DownloadPage {...attrs} />}
          {page !== downloadPage && (
            <Grid container direction="row" justify="flex-end" style={{ marginTop: '2.5rem' }}>
              {this.state.page !== 1 && (
                <Button
                  data-cy="prev"
                  ghost
                  large
                  onClick={() => this._changePage('previous')}
                  type="button"
                >
                  Previous
                </Button>
              )}
              <Button
                data-cy="save-draft"
                disabled={this.state.draftLoading}
                isloading={this.state.draftLoading}
                large
                onClick={() => this._handleSaveDraft()}
                type="button"
              >
                Save Draft
              </Button>
              {(page === lastPageReject && values.decision !== 'Accept') || page === lastPageAccept ? (
                <Button
                  data-cy="submit"
                  disabled={isSubmitting || values.decision === ''}
                  isloading={isSubmitting}
                  large
                  onClick={this._handleBeforeSubmitting}
                  type="button"
                >
                  Submit
                </Button>
              ) : (
                <Button data-cy="next" large onClick={() => this._changePage('next')} type="button">
                  Next
                </Button>
              )}
            </Grid>
          )}
        </form>
      </>
    );
  }
}

const MyEnhancedForm = withFormik({
  enableReinitialize: true,
  validationSchema: validations,
  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 400);
  },
  displayName: 'BusinessIdeaForm',
})(UserForm);

UserForm.propTypes = {
  errors: propTypes.object.isRequired,
  getPitchingLocation: propTypes.func.isRequired,
  getTribe: propTypes.func.isRequired,
  handleSendForm: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired,
  id: propTypes.number.isRequired,
  isSubmitting: propTypes.bool.isRequired,
  isValid: propTypes.bool.isRequired,
  onFilesAdded: propTypes.func.isRequired,
  onFilesRemoved: propTypes.func.isRequired,
  openSnackbar: propTypes.func.isRequired,
  setFieldTouched: propTypes.func.isRequired,
  setFieldValue: propTypes.func.isRequired,
  touched: propTypes.object.isRequired,
  values: propTypes.object.isRequired,
};

export default MyEnhancedForm;
