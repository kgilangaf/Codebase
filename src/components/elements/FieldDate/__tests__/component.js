import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import Component from '../component';

describe('Field Date', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<Component long required={false} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders with error', () => {
    const tree = renderer
      .create(<Component block error="this is error message" required small touched />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Change Value', () => {
    let value = {
      startDate: new Date(),
      endDate: new Date()
    };

    const wrapper = shallow(<Component required value={value} />);
    wrapper.find('.cdx-input').simulate('click', {
      preventDefault: jest.fn()
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('Change Value with String Value', () => {
    let value = {
      startDate: new Date(),
      endDate: new Date()
    };

    const wrapper = shallow(<Component required useFormattedValue value={value} />);
    wrapper.find('.cdx-input').simulate('click', {
      preventDefault: jest.fn()
    });
    expect(wrapper).toMatchSnapshot();
  });
});
