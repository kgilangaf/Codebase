import React from 'react';
import renderer from 'react-test-renderer';
import Breadcrumb from '../index';

jest.mock('react-router-dom', () => ({
  Link: (props) => `Link-${props.children}`,
}));

let data = [];
let testLink = {};

describe('Breadcrumb', () => {
  beforeEach(() => {
    data = [
      { text: [{}], url: 'test' },
      { text: 'Test Text' },
    ];
  });

  it('renders correctly', () => {
    const tree = renderer
      .create(<Breadcrumb data={data} />)
      .toJSON();
    const { children } = tree;

    expect(children.length).toBe(3);
    expect(children[0].children[0]).toBe(`Link-${testLink}`);
  });

  it('use custom class', () => {
    const tree = renderer
      .create(<Breadcrumb className="customClass" data={data} />)
      .toJSON();
    const { props: { className } } = tree;

    expect(className.split(' ')).toContain('customClass');
  });
});
