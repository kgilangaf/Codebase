import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styles from './styles.css';

export default class Breadcrumb extends React.Component {
  _renderText(item, idx) {
    const { text, url } = item;

    return (
      <div key={idx}>
        {
          url ? (
            <Link
              className={styles.link}
              to={url}
            >
              {  typeof(text)==='object' ?
                <h5 className={styles.icon}>
                  {text}
                </h5>
                :
                <h5 className={url ? styles.link : styles.text}>{text}</h5>
              }
            </Link>
          ) : <h5 className={styles.text}>{text}</h5>
        }
      </div>
    );
  }

  render() {
    const { className, data } = this.props;
    const customClass = className ? `${styles.root} ${className}` : styles.root;
    const separator = (key) => (
      <h5 className={styles.textSeparator} key={`spt-${key}`}> {key >= 1 ? ` / ` : ` `}</h5>
    );    

    return (
      <nav className={customClass}>
        {
          data
            .map((item, idx) => this._renderText(item, idx))
            .reduce((prev, curr, idx) => [prev, separator(idx), curr])
        }
      </nav>
    );
  }
}

Breadcrumb.defaultProps = {
  className: '',
  data: [],
};

Breadcrumb.propTypes = {
  className: PropTypes.string,
  data: PropTypes.array,
};
