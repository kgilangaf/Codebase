import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions
} from '../../elements/Dialog';
import Button from '../Button';
import { storiesOf } from '@storybook/react';
import { State, Store } from '@sambego/storybook-state';

const store = new Store({
  open: true
});

const actions = {
  _handleOnClose: () => {
    store.set({ open: false });
  },
};

storiesOf('Modal', module)
  .add('Default', () => (
    <State store={store}>
      <Dialog
        aria-describedby="alert-dialog-description"
        aria-labelledby="alert-dialog-title"
        onClose={actions._handleOnClose}
        {...store}
      >
        <DialogTitle id="alert-dialog-title">Are you sure want to delete the proposal?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This process can’t be undone.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus ghost onClick={actions._handleOnClose} type="button">
            Cancel
          </Button>
          <Button autoFocus type="button">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </State>
  ));
