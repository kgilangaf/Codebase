import React from 'react';
import PropTypes from 'prop-types';
import DialogContentText from '@material-ui/core/DialogContentText';
import styles from '../styles.css';

export default class Component extends React.Component {
  render() {
    return (
      <DialogContentText {...this.props} className={styles['modal-content-text']}>
        {this.props.children}
      </DialogContentText>
    );
  }
}

Component.defaultProps = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};

Component.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string,
  ]),
};
