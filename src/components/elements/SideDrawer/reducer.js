import { ACTIONS } from '../../../constants';

const initialData = {
  isOpen: false,
  componentName: '',
};

export function sideDrawerData(state = initialData, action) {
  switch (action.type) {
    case ACTIONS.TOGGLE_SIDE_DRAWER_SUCCESS:
      return {
        isOpen: action.isOpen,
        componentName: action.componentName,
      };
    default:
      return state;
  }
}
