import { connect } from 'react-redux';
import SnackbarContainer from './SnackbarContainer';

function mapStateToProps(state) {
  return {
    snackbar: {
      isOpen: state.snackbar.isOpen,
      message: state.snackbar.message,
      variant: state.snackbar.variant
    }
  };
}

const SnackbarContainerWithRedux = connect(mapStateToProps, null)(SnackbarContainer);

export default SnackbarContainerWithRedux;
