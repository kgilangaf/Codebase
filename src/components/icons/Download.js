import React from 'react';
import PropTypes from 'prop-types';

export default function Download(props) {
  const { color, width, height } = props;

  return (
    <svg
      height={height}
      style={{ verticalAlign: 'middle', marginRight: '0.0625rem' }}
      viewBox="0 0 24 24"
      width={width}
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="none" fillRule="evenodd" id="icons" stroke="none" strokeWidth="1">
        <g id="icon" transform="translate(-147.000000, -88.000000)">
          <g id="Download-thin" transform="translate(147.000000, 88.000000)">
            <rect fill={color} height="24" id="kotak-bening" opacity="0" width="24" x="0" y="0" />
            <path
              d="M19.76,19.02 L19.76,13.62 C19.76,13.2775835 20.0375835,13 20.38,13 C20.7224165,13 21,13.2775835 21,13.62 L21,20.26 C21,20.6686907 20.6686907,21 20.26,21 L3.74,21 C3.33130929,21 3,20.6686907 3,20.26 L3,13.62 C3,13.2775835 3.27758346,13 3.62,13 C3.96241654,13 4.24,13.2775835 4.24,13.62 L4.24,19.02 C4.24,19.4286907 4.57130929,19.76 4.98,19.76 L19.02,19.76 C19.4286907,19.76 19.76,19.4286907 19.76,19.02 Z M12.63,14.6156176 L15.6342763,11.6113413 C15.8764014,11.3692162 16.2689637,11.3692162 16.5110887,11.6113413 C16.7532138,11.8534663 16.7532138,12.2460286 16.5110887,12.4881537 L12.5230065,16.4762359 C12.2340185,16.7652239 11.7654764,16.7652239 11.4764885,16.4762359 L7.4884062,12.4881537 C7.24628114,12.2460286 7.24628114,11.8534663 7.4884062,11.6113413 C7.73053127,11.3692162 8.12309355,11.3692162 8.36521861,11.6113413 L11.39,14.6361227 L11.39,3.62 C11.39,3.27758346 11.6675835,3 12.01,3 C12.3524165,3 12.63,3.27758346 12.63,3.62 L12.63,14.6156176 Z"
              fill={color}
            />
          </g>
        </g>
      </g>
    </svg>
  );
}

Download.defaultProps = {
  color: '#1E2025',
  height: '1rem',
  width: '1rem'
};

Download.propTypes = {
  color: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
};
