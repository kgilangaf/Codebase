import { createMuiTheme } from '@material-ui/core/styles';
import button from './CustomStyle/button';

export const theme = createMuiTheme({
  overrides: {
    MuiButtonBase: button,
  },
  palette: {
    primary: {
      main: '#289F97',
    },
  },
  typography: {
    fontFamily: 'ubuntu !important',
  },
});

export const COLOR_PRIMARY = '#e141f2';
export const COLOR_PRIMARY_2 = '#ff00ff';
export const COLOR_WHITE = '#ffffff';
export const FONT =  'ubuntu';
