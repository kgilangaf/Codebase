import { ACTIONS } from '../../constants';

const initialState = {
  isLoading: {
    details: true,
    talent: true,
    achievement: true,
  },
  data: {
    details: null,
    talent: null,
    achievement: null,
  },
};

export default function reducer(state = initialState, action) {
  const { type, data, name } = action;
  switch (type) {
    case ACTIONS.LOADING:
      return {
        ...state,
        isLoadingTribe: {
          ...state.isLoadingTribe,
          [name]: true,
        },
      };
    case ACTIONS.LIST_PROJECT_DETAILS_FETCHED:
      return {
        ...state,
        isLoading: {
          ...state.isLoading,
          [name]: false,
        },
        data: {
          ...state.data,
          [name]: data,
        },
      };
    default:
      return state;
  }
}
