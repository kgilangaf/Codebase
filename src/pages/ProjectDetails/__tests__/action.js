import * as actions from '../action';
import fetch from '../../../utils/fetch';

jest.mock('../../../configs');
jest.mock('../../../utils/fetch');

let dispatch;
let newQuery = '';

describe('Actions', () => {
  beforeEach(() => {
    dispatch = jest.fn();
    const mockFn = (param) => new Promise((resolve, reject) => {
      if (param.method === 'get') resolve({ data: {}, });
      if (param.data && param.data.status === 'test' || param.meta === 'test') resolve({ data: 'success', meta: 'test' });
      else reject({ data: { message: 'fail' } });
      
    });
    newQuery = { search: '' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: newQuery,
    });
    fetch.mockImplementation(mockFn);
  });

  it('calls dispatch when fetchData success', async () => {
    const mockFn = () => new Promise((resolve) => {
      resolve({ data: {}, meta: {} });
    });

    fetch.mockImplementation(mockFn);
    expect.assertions(1);
    await actions.fetchData()(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch when fetchData success with Query', async () => {
    const mockFn = () => new Promise((resolve) => {
      resolve({ data: {}, meta: {} });
    });
    newQuery = { search: '?category=date&count=10' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: { search: newQuery.search }
    });

    fetch.mockImplementation(mockFn);
    expect.assertions(1);
    await actions.fetchData()(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch when setQuery success', async () => {
    const param = { name: 'test', value: 'test' };

    expect.assertions(1);
    await actions.setQuery(1, param)(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch when setQuery success and data is Date', async () => {
    const param = { 
      isDate: true,
      from: new Date('03/12/2019'),
      to: new Date('03/12/2019'),
    };

    expect.assertions(1);
    await actions.setQuery(1, param)(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });

  it('calls dispatch when setQuery success and category is date', async () => {
    const param = { name: 'test', value: 'test' };
    newQuery = { search: '?category=date' };

    Object.defineProperty(window, 'location', {
      writable: true,
      value: { search: newQuery.search }
    });

    expect.assertions(1);
    await actions.setQuery(1, param)(dispatch);

    expect(dispatch).toHaveBeenCalled();
  });
});
