import reducer from '../reducer';
import { ACTIONS } from '../../../constants';

describe('Reducer', () => {
  it('returns desired state when given LOGIN_FAILED action', () => {
    const action = {
      type: ACTIONS.LOGIN_FAILED,
      message: 'Login Failed',
    };
    const assert = {
      message: 'Login Failed',
    };
    expect(reducer(undefined, action)).toMatchObject(assert);
  });
  it('returns initial state when given other actions', () => {
    const action = { type: 'x' };
    const assert = {
      message: ''
    };
    expect(reducer(undefined, action)).toMatchObject(assert);
  });
});
