export function currency(value = 0) {
  if (!value) return 'Rp 0';

  const format = Math.floor(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

  return `Rp ${format}`;
}

export function numberWithPoint(x) {
  const parts = x.toString().split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return parts.join('.');
}
